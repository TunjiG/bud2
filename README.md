1. Install composer - https://getcomposer.org/download/
2. Install dependencies - `php composer.phar install`
3. Execute test - `bin/phpspec --verbose run`