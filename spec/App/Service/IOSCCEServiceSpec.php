<?php

namespace spec\App\Service;

require 'vendor/autoload.php';

use App\Service\IOSCCEService;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\TransferException;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use GuzzleHttp\Psr7\Response;

class IOSCCEServiceSpec extends ObjectBehavior
{
    function let(Client $client)
    {
        $this->beConstructedWith($client);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(IOSCCEService::class);
    }

    function it_successfully_retrieves_prisoner_info(Client $client, Response $response)
    {
        $token = 'A-TOKEN';
        $json = '
        { 
            "cell": "01000011 01100101 01101100 01101100 00100000 00110010 00110001 00111000 00110111",
            "block": "01000100 01100101 01110100 01100101 01101110 01110100 01101001 01101111 01101110 00100000 01000010 01101100 01101111 01100011 01101011 00100000 01000001 01000001 00101101 00110010 00110011 00101100"
        }';

        $response->getBody()->willReturn($json);

        $client->request('GET', '/prisoner/leia', [
            'headers' => [
                'Content-Type'     => 'application/json',
                'Authorization'     => 'Bearer A-TOKEN'
            ]
        ])->shouldBeCalled()->willReturn($response);

        $response = $this->getPrisoner('leia', $token);

        $response->shouldEqual(['cell' => 'C e l l   2 1 8 7', 'block' => 'D e t e n t i o n   B l o c k   A A - 2 3 ,']);
    }

    function it_does_not_have_expected_properties(Client $client, Response $response)
    {
        $token = 'A-TOKEN';
        $json = '
        { 
            "error": "B a d  r o b o t"
        }';

        $response->getBody()->willReturn($json);

        $client->request('GET', '/prisoner/leia', [
            'headers' => [
                'Content-Type'     => 'application/json',
                'Authorization'     => 'Bearer A-TOKEN'
            ]
        ])->shouldBeCalled()->willReturn($response);

        $response = $this->getPrisoner('leia', $token);

        $response->shouldEqual(['error' => 'B a d  r o b o t']);
    }
}
