<?php

namespace App\Service;

require 'vendor/autoload.php';

use GuzzleHttp\Client;
use GuzzleHttp\Exception\TransferException;

class IOSCCEService
{
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function getPrisoner($name, $token)
    {
        $prisonerResponse = $this->client->request('GET', sprintf('/prisoner/%s', $name), [
            'headers' => [
                'Content-Type'     => 'application/json',
                'Authorization'     => sprintf('Bearer %s', $token)
            ]
        ]);

        $responseArray = json_decode($prisonerResponse->getBody(), true);

        if (isset($responseArray['cell']) && isset($responseArray['block'])) {
            $responseArray['cell'] = $this->binaryTextToAscii($responseArray['cell']);
            $responseArray['block'] = $this->binaryTextToAscii($responseArray['block']);
        }

        return $responseArray;
    }

    private function binaryTextToAscii($string)
    {
        $parts = explode(' ', $string);

        foreach ($parts as $i => $bin) {
            $parts[$i] = chr(bindec($bin));
        }

        return implode(' ', $parts);
    }
}